FROM node:10.22.0-alpine3.11
RUN mkdir -p /node/suma
WORKDIR /node/suma
COPY . .
RUN npm install
EXPOSE 3000
CMD ["node", "index.js"]