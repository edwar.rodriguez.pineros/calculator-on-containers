//import express from express;
const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const cors = require("cors");
const port = 3000;

// parse application/json
app.use(express.json())
app.use(cors({
    origin: '*', //ruta desde donde se hacen las peticiones
    credentials: true
  })); 
app.use(bodyParser.json())

// Ruta para peticiones HTTP: get, post, put ...
app.post('/sumar', function (peticion, respuesta){

    let number1 = peticion.body.number1;
    let number2 = peticion.body.number2;
    
    if(isNaN(number1) || isNaN(number2)){
        respuesta.status(400).json({message :'Numeros inválidos'});
    } else {
        let suma = parseFloat(number1) + parseFloat(number2);
        respuesta.json({ resultado : suma });
    }
    
});

// "prender" un servidor, Escuchar peticiones 

app.listen(port, () => {
  console.log(`Servidor activo en http://localhost:${port}`)
})